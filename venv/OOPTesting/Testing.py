from OOPTesting.Bank import *

#Input from user
name = input('Enter your name: ')
age = input('Enter your age: ')
gender = input('Enter your gender: ')

#View User Info
bankUser = Bank()
bankUser.setName(name)
bankUser.setAge(age)
bankUser.setGender(gender)
bankUser.show_details()

#User input deposit value
deposit = input("Enter deposit amount: ")
bankUser.deposit(int(deposit))
bankUser.show_details()

#User withdraw balance
withDraw = input("Enter withdraw amount: ")
bankUser.withdraw(int(withDraw))
bankUser.show_details()
