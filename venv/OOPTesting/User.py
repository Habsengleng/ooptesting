class User:
    def __init__(self):
        self.__name = ''
        self.__age = ''
        self.__gender = ''

    def setName(self,name):
        self.__name=name

    def setAge(self,age):
        self.__age=age

    def setGender(self,gender):
        self.__gender=gender

    def show_details(self):
        print("Personal Details")
        print("")
        print("Name ", self.__name)
        print("Age  ", self.__age)
        print("Gender ", self.__gender)