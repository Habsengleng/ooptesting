from OOPTesting.User import *

class Bank(User):
    def __init__(self):
        super().__init__()
        self.balance = 0

    def deposit(self, amount):
        self.amount = amount
        self.balance = self.balance + self.amount
        print("Account balance has been updated : $", self.balance)

    def withdraw(self, amount):
        self.amount = amount
        if self.amount > self.balance:
            print("Insufficient Funds | Balance Available : $", self.balance)
        else:
            self.balance = self.balance - self.amount
            print("Account balance has been updated : $", self.balance)

    def show_details(self):
        super(Bank, self).show_details()
        print("Account balance: $", self.balance)
